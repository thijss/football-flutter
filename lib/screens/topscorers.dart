import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:http/http.dart' as http;

class Topscorers extends StatefulWidget {
  @override
  TopscorersState createState() => TopscorersState();
}

// Create the state for our stateful widget
class TopscorersState extends State<Topscorers> {
  List topscorersData;
  Map playerDetailData;

  Future<String> getTopScorersData() async {
    final String url = "https://football.thijss.nl/api/players/";
    var response = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
    setState(() {
      topscorersData = json.decode(response.body);
    });
    return "Successfull";
  }

  Future<String> getPlayerDetailData(pk) async {
    final String url = "https://football.thijss.nl/api/players/" + pk.toString();
    var response = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
    setState(() {
      playerDetailData = json.decode(response.body);
    });
    return "Successfull";
  }

  Widget playerDetailPage() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              padding: const EdgeInsets.only(top: 32),
              child: Center(
                child: Text(
                  playerDetailData["name"],
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                ),
              )),
          SizedBox(
              height: 500,
              child: ListView.builder(
                  padding: const EdgeInsets.all(16.0),
                  itemCount: playerDetailData == null
                      ? 1
                      : playerDetailData["matches"].length + 1,
                  itemBuilder: (BuildContext _context, int index) {
                    if (index == 0) {
                      // return the header
                      return ListTile(
                          title: Text("Match",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          trailing: Text("Goals",
                              style: TextStyle(fontWeight: FontWeight.bold)));
                    }
                    index -= 1;
                    var match = playerDetailData["matches"][index];
                    var goalList = new List<String>.generate(
                        match["goals"], (int index) => "\u{26BD}");
                    return ListTile(
                      title: Text(match["title"] + " (" + match["score"] + ")"),
                      trailing: Text(goalList.join()),
                    );
                  }))
        ]);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: topscorersData == null ? 0 : topscorersData.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            child: Center(
                child: Column(
              // Stretch the cards in horizontal axis
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Card(
                  child: ListTile(
                    title: Text(topscorersData[index]['name']),
                    trailing: Badge(
                        badgeColor: Colors.indigo,
                        padding: const EdgeInsets.all(8),
                        badgeContent: Text(
                            topscorersData[index]['total_goals'].toString(),
                            style: TextStyle(color: Colors.white))),
                    onTap: () {
                      var pk = topscorersData[index]['pk'];
                      _pushPlayerDetail(pk);
                    },
                  ),
                ),
              ],
            )),
          );
        });
  }

  void _pushPlayerDetail(pk) {
    Navigator.of(context)
        .push(MaterialPageRoute<void>(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(title: Text('Player Details')),
        body: FutureBuilder(
            future: getPlayerDetailData(pk),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return playerDetailPage();
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            }),
      );
    }));
  }

  @override
  void initState() {
    super.initState();

    // Call the getTopScorersData() method when the app initializes
    getTopScorersData();
  }
}
