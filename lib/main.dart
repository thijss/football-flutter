
import 'package:flutter/material.dart';
import 'package:voetbal/screens/account.dart';
import 'package:voetbal/screens/home.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      primarySwatch: Colors.indigo,
    ),
    home: HomeScreen(), // route for home is '/' implicitly
    routes: <String, WidgetBuilder>{
      // define the routes
      AccountScreen.routeName: (BuildContext context) => AccountScreen(),
    },
  ));
}